package course.examples.practica_06;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {


    //variables para cada componente del layout
    Button save,load;
    EditText message1,message2,message3;
    String Message1,Message2,Message3;

    //variable para el tamaño de la informción guardada
    int data_block = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // guardar en las variables lo referente a cada componente
        save = (Button)findViewById(R.id.SAVE);
        load = (Button)findViewById(R.id.LOAD);
        message1 = (EditText)findViewById(R.id.MSG_NAME);
        message2 = (EditText)findViewById(R.id.MSG_EMAIL);
        message3 = (EditText)findViewById(R.id.MSG_PHONE);

        // listener para guardar la información
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message1 = message1.getText().toString();
                Message2 = message2.getText().toString();
                Message3 = message3.getText().toString();
                try {

                    //Crear un archivo
                    FileOutputStream fou = openFileOutput("text.txt",MODE_WORLD_READABLE);
                    OutputStreamWriter osw = new OutputStreamWriter(fou);
                    try {
                        //Escribir en el archivo
                        osw.write(Message1+", "+Message2+", "+ Message3);
                        //osw.write(Message1);
                        //osw.write(Message2);
                        //osw.write(Message3);
                        //osw.write("abc");
                        osw.flush();
                        osw.close();
                        Toast.makeText(getBaseContext(),"Data saved...",Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        //listener para cargar información almacenada
        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //leer el archivo
                    FileInputStream fis = openFileInput("text.txt");
                    InputStreamReader isr = new InputStreamReader(fis);
                    //crear variable para guardar información
                    char[] data = new char[data_block];
                    String final_data ="";
                    int size;
                    try {

                        //leer y asignar a arreglo el contenido del archivo de texto
                        while((size = isr.read(data))>0){
                            String read_data = String.valueOf(data,0,size);
                            final_data += read_data;
                            data = new char[data_block];
                        }
                        //Desplegar mensaje
                        Toast.makeText(getBaseContext(),"Contact info: "+final_data,Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }




}
